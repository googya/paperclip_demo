class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :data_file_name
      t.string :data_content_type
      t.integer :data_file_size
      t.datetime :data_updated_at
      t.integer :user_id
      t.string :file_type

      t.timestamps
    end
  end
end
