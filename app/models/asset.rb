class Asset < ActiveRecord::Base
  attr_accessible :data_content_type, :data_file_name, :data_file_size, :data_updated_at, :file_type, :user_id
 
  belongs_to :user
  
  has_attached_file :data, :url => "/system/:class/:attachment/:id/:filename",
                           :styles => {:medium => "300x300>", :thumb => "100x100>", :big => "1000x1000"}  
  
  before_post_process :crop_pic
  
  def crop_pic
    data_content_type.include?("image")
  end
    
end
