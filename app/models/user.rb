class User < ActiveRecord::Base
  attr_accessible :age, :info, :name, :assets
  has_many :assets
end
